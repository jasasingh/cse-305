<!DOCTYPE html>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome!</title>
    <style type="text/css">
    td
    {
        padding:0 15px 0 15px;
    }
    </style>
</head>
<body>

<h1> User: <c:out value="${emailId}"/>! </h1>
<h3><a href="<c:url value="/logout" />">Logout</a></h3>

<table>
    <tr>
        <td>
            <form method="get" action="/customer/welcome">
                <input type="submit" value="Home" class="btn btn-success btn-sm" />
            </form>
        </td>
        <!--<td>
            <form method="get" action="/customer/info">
                <input type="submit" value="Account Info" class="btn btn-success btn-sm" />
            </form>
        </td>-->
        <td>
            <form method="get" action="/cart/get">
                <input type="submit" value="Cart" class="btn btn-success btn-sm" />
            </form>
        </td>
    </tr>
</table>

<table>
    <tr>
        <th>Order ID</th>
        <th>Order Date</th>
        <th>Shipping Address</th>

        <th>Shipping Fee</th>
        <th>Details</th>
    </tr>


    <c:forEach items="${order_records}" var="item">

        <tr>
            <td>
            <c:out value="${item.id}" />
            </td>
            <td>
            <c:out value="${item.orderDate}" />
            </td>
            <td>
            <c:out value="${item.address}" />
            </td>
            <td style="text-align:right">
            $<c:out value="${item.shippingFee}" />
            </td>
            <td>
            <c:url var="orderItemPage" value="/orders/orderitems">
                <c:param name="order_record_id" value="${item.id}" />
            </c:url>
            <a href="${orderItemPage}">See Order Details</a>
            </td>

        </tr>


    </c:forEach>
</table>

</body>
</html>