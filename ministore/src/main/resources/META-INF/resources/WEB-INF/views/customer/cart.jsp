<!DOCTYPE html>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome!</title>
    <style type="text/css">
    td
    {
        padding:0 15px 0 15px;
    }
    </style>
</head>
<body>

<h1> User: <c:out value="${emailId}"/>! </h1>
<h3><a href="<c:url value="/logout" />">Logout</a></h3>

<table>
    <tr>
        <td>
            <form method="get" action="/customer/welcome">
                <input type="submit" value="Home" class="btn btn-success btn-sm" />
            </form>
        </td>
        <!--<td>
            <form method="get" action="/customer/info">
                <input type="submit" value="Account Info" class="btn btn-success btn-sm" />
            </form>
        </td>-->
        <td>
            <form method="get" action="/orders">
                <input type="submit" value="Orders History" class="btn btn-success btn-sm" />
            </form>
        </td>
    </tr>
</table>

<table>
    <tr>
        <th>Product</th>
        <th>Price</th>
        <th>Seller</th>
        <th>Quantity</th>
    </tr>

    <c:forEach items="${cart_items}" var="item">
       <tr>
            <td>
            <c:out value="${item['name']}" />
            </td>
            <td style="text-align:right">
            $<c:out value="${item['price']}" />
            </td>
            <td>
            <c:out value="${item['seller_name']}" />
            </td>
            <td>
            <c:out value="${item['quantity']}"></c:out>
            </td>
            <input type="hidden" value ="${item['id']}" name = "item_id"></input>

        </tr>
    </c:forEach>


</table>




<form method="get" action="/cart/checkout">
    <input type="submit" value="Checkout" class="btn btn-success btn-sm" />
</form>

</body>
</html>