<!DOCTYPE html>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<head>
    <title>Sign up</title>
    <link rel="icon" type="/image/png" href="../img/favicon.ico    " />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="../css/general.css" rel="stylesheet">

    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="wrap" style="padding-top:15%">
                    <form:form class="login" modelAttribute="customerForm" method="post" action="/signup/customer">
                        <img src="../img/logo.png" class="img-responsive form-title" alt="MiniStore" style="margin:auto"/>
                        <p class="form-title">Sign Up</p>

                        <spring:bind path="emailId">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="email" path="emailId" placeholder="Email" autofocus="true"></form:input>
                                <form:errors path="emailId"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="password">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="password" path="password" placeholder="Password"></form:input>
                                <form:errors path="password"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="person.firstName">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="text" path="person.firstName"  placeholder="First Name" ></form:input>
                                <form:errors path="person.firstName"></form:errors>

                            </div>
                        </spring:bind>

                        <spring:bind path="person.lastName">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="text" path="person.lastName" placeholder="Last Name"></form:input>
                                <form:errors path="person.lastName"></form:errors>
                            </div>
                        </spring:bind>


                        <spring:bind path="person.address.streetAddress">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="text" path="person.address.streetAddress" placeholder="Street Address"></form:input>
                                <form:errors path="person.address.streetAddress"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="person.address.city">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="text" path="person.address.city" placeholder="City"></form:input>
                                <form:errors path="person.address.city"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="person.address.state">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="text" path="person.address.state" placeholder="State"></form:input>
                                <form:errors path="person.address.state"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="person.address.zipCode">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="number" path="person.address.zipCode" placeholder="Zip Code"></form:input>
                                <form:errors path="person.address.zipCode"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="person.phoneNumber">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="tel" path="person.phoneNumber" placeholder="Phone Number"></form:input>
                                <form:errors path="person.phoneNumber"></form:errors>
                            </div>
                        </spring:bind>



                            <input type="submit" value="Sign Up" class="btn btn-success btn-sm" />


                    </form:form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>