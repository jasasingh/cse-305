<!DOCTYPE html>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<head>
    <title>Sign up</title>
    <link rel="icon" type="/image/png" href="../img/favicon.ico    " />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="../css/general.css" rel="stylesheet">

    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="wrap" style="padding-top:15%">
                    <form:form class="login" modelAttribute="sellerForm" method="post" action="/signup/seller">
                        <img src="../img/logo.png" class="img-responsive form-title" alt="MiniStore" style="margin:auto"/>
                        <p class="form-title">Sign Up</p>

                        <spring:bind path="emailId">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="email" path="emailId" placeholder="Email" autofocus="true"></form:input>
                                <form:errors path="emailId"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="password">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="password" path="password" placeholder="Password"></form:input>
                                <form:errors path="password"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="companyName">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="text" path="companyName" placeholder="Company Name"></form:input>
                                <form:errors path="companyName"></form:errors>
                            </div>
                        </spring:bind>
                        <spring:bind path="address.streetAddress">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="text" path="address.streetAddress" placeholder="Street Address"></form:input>
                                <form:errors path="address.streetAddress"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="address.city">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="text" path="address.city" placeholder="City"></form:input>
                                <form:errors path="address.city"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="address.state">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="text" path="address.state" placeholder="State"></form:input>
                                <form:errors path="address.state"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="address.zipCode">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="number" path="address.zipCode" placeholder="Zip Code"></form:input>
                                <form:errors path="address.zipCode"></form:errors>
                            </div>
                        </spring:bind>

                        <spring:bind path="phoneNumber">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="tel" path="phoneNumber" placeholder="Phone Number"></form:input>
                                <form:errors path="phoneNumber"></form:errors>
                            </div>
                        </spring:bind>

                        <input type="submit" value="Sign Up" class="btn btn-success btn-sm" />
                    </form:form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>