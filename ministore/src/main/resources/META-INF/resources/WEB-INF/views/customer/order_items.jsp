<!DOCTYPE html>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome!</title>
    <style type="text/css">
    td
    {
        padding:0 15px 0 15px;
    }
    </style>
</head>
<body>

<h1> User: <c:out value="${emailId}"/>! </h1>
<h3><a href="<c:url value="/logout" />">Logout</a></h3>




<table>
    <tr>
        <td>
            <form method="get" action="/customer/welcome">
                <input type="submit" value="Home" class="btn btn-success btn-sm" />
            </form>
        </td>
        <!--<td>
            <form method="get" action="/customer/info">
                <input type="submit" value="Account Info" class="btn btn-success btn-sm" />
            </form>
        </td>-->
        <td>
            <form method="get" action="/cart/get">
                <input type="submit" value="Cart" class="btn btn-success btn-sm" />
            </form>
        </td>
        <td>
            <form method="get" action="/orders">
                <input type="submit" value="Orders History" class="btn btn-success btn-sm" />
            </form>
        </td>
    </tr>
</table>

<table>
    <tr>
        <th>Product</th>
        <th>Price</th>
        <th>Quantity</th>

        <th>Seller</th>
    </tr>



<c:forEach items="${order_items}" var="item">
    <tr>

    <td>
    <c:out value="${item.id}" />
    </td>
    <td style="text-align:right">
    $<c:out value="${item.price}" />
    <td>
    <c:out value="${item.quantity}" />
    </td>
    <td>
    <c:out value="${item.sellerName}" />
    </td>
    <!--<input type="submit" value="-" class="btn btn-success btn-sm" /> -->
    </tr>

</c:forEach>

</table>


</body>
</html>