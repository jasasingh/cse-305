<!DOCTYPE html>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<head>
    <title>Sign up</title>
    <link rel="icon" type="/image/png" href="../img/favicon.ico    " />

</head>
<body>
<h1> User: <c:out value="${emailId}"/>! </h1>
<h3><a href="<c:url value="/logout" />">Logout</a></h3>

<table>
    <tr>
        <td>
            <form method="get" action="/customer/welcome">
                <input type="submit" value="Home" class="btn btn-success btn-sm" />
            </form>
        </td>
        <td>
            <form method="get" action="/orders">
                <input type="submit" value="Orders History" class="btn btn-success btn-sm" />
            </form>
        </td>
    </tr>
</table>

<h1>Add a Payment Method</h1>


                <form:form class="login" modelAttribute="paymentInfo" method="post" action="/customer/addPayment">
                    <spring:bind path="creditCardNumber">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <form:input type="number" path="creditCardNumber" placeholder="Credit Card #" autofocus="true"></form:input>
                            <form:errors path="creditCardNumber"></form:errors>
                        </div>
                    </spring:bind>

                    <spring:bind path="expirationDate">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <form:input type="date" path="expirationDate" placeholder="Expiration Date"></form:input>
                            <form:errors path="expirationDate"></form:errors>
                        </div>
                    </spring:bind>

                    <input type="submit" value="Enter Payment Info" class="btn btn-success btn-sm" />


                </form:form>


</body>
</html>