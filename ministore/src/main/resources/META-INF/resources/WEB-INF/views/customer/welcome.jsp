<!DOCTYPE html>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome!</title>
    <style type="text/css">
        td
        {
            padding:0 15px 0 15px;
        }
        </style>
</head>
<body>

<h1> Welcome <c:out value="${emailId}"/>!</h1>
<h3><a href="<c:url value="/logout" />">Logout</a></h3>

<table>
    <tr>
        <td>
            <form method="get" action="/cart/get">
                <input type="submit" value="Go To Cart" class="btn btn-success btn-sm" />
            </form>
        </td>
        <!--<td>
            <form method="get" action="/customer/info">
                <input type="submit" value="Account Info" class="btn btn-success btn-sm" />
            </form>
        </td>-->
        <td>
            <form method="get" action="/orders">
                <input type="submit" value="Orders History" class="btn btn-success btn-sm" />
            </form>
        </td>
    </tr>
</table>

<table>
    <tr>
        <th>Product</th>
        <th>Price</th>
        <th>Seller</th>

        <th>Add to Cart</th>
    </tr>


    <c:forEach items="${inventory}" var="item">
            <tr>
                <td>
                <c:out value="${item['name']} " />
                </td>
                <td style="text-align:right">
                $<c:out value="${item['price']}" />
                </td>
                <td>
                <c:out value="${item['seller_name']}" />
                </td>
                <td>
                <c:url var="addToCartPage" value="/cart/add">
                    <c:param name="item_id" value="${item['id']}" />
                </c:url>
                <a href="${addToCartPage}">Add to Cart</a>
                </td>
             </tr>
    </c:forEach>

</table>

</body>
</html>