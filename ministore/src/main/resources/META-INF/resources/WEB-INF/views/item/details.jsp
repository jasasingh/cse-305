<!DOCTYPE html>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome!</title>
</head>
<body>

<h1> User: <c:out value="${emailId}"/>! </h1>

<table>
    <tr>
        <td>
            <form method="get" action="/customer/welcome">
                <input type="submit" value="Home" class="btn btn-success btn-sm" />
            </form>
        </td>
        <td>
            <form method="get" action="/customer/info">
                <input type="submit" value="Account Info" class="btn btn-success btn-sm" />
            </form>
        </td>
        <td>
            <form method="get" action="/customer/cart">
                <input type="submit" value="Go To Cart" class="btn btn-success btn-sm" />
            </form>
        </td>
        <td>
            <form method="get" action="/customer/orders">
                <input type="submit" value="Order Record" class="btn btn-success btn-sm" />
            </form>
        </td>
    </tr>
</table>

<table>
    <tr>
        <th>Item</th>
        <th>CustomerID</th>
        <th>Rating</th>

        <th>Review</th>
    </tr>

    <c:forEach items="${ItemReview}" var="item">
        <tr>
            <td><c:out value="${itemReview['name']} " /></td>
            <td><c:out value="${itemReview['customer_id']}" /></td>
            <td><c:out value="${itemReview['rating']}" /></td>
            <td><c:out value="${itemReview['Review']}"></c:out></td>
        </tr>
    </c:forEach>

</table>


<form method="POST" action="/item/details">
    <input type="text" value="Review" class="btn btn-success btn-sm" />
</form>

</body>
</html>