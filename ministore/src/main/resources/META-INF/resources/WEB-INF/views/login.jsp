<!DOCTYPE html>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<head>
    <title>Log In</title>
    <link rel="icon" type="/image/png" href="img/favicon.ico    " />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="css/general.css" rel="stylesheet">

    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

    <script src="js/login.js"></script>
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="wrap" style="padding-top:15%">
                    <form class="login" method="post" action="/login">
                        <img src="img/logo.png" class="img-responsive form-title" alt="MiniStore" style="margin:auto"/>
                        <p class="form-title">Sign In</p>
                        <input type="email" placeholder="Email" name="emailId"/>
                        <input type="password" placeholder="Password" name="password"/>
                        <input type="submit" value="Login" class="btn btn-success btn-sm" />
                        <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}"/>
                    </form>
                    <form class="login" method="get" action="/signup/customer">
                        <input type="submit" value="Sign Up - Customer" class="btn btn-success btn-block" />
                    </form>
                    <form class="login" method="get" action="/signup/seller">
                        <input type="submit" value="Sign Up - Seller" class="btn btn-success btn-block" />
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>