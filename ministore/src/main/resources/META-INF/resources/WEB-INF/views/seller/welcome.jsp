<!DOCTYPE html>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Seller Dashboard!</title>
</head>
<body>

<h1> Welcome Dear Seller: <c:out value="${emailId}"/>!</h1>
<h3><a href="<c:url value="/logout" />">Logout</a></h3>
You are a seller!

</body>
</html>