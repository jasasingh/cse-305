package com.ministore.ministore.repositories.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class RepositoryCommons {

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryCommons.class);

    public static <E> E findByUniqueAttribute(JdbcTemplate jdbcTemplate, String tableName, String uniqueAttribute, Object argument, RowMapper<E> rowMapper) {
        List<E> results = findAllByAttribute(jdbcTemplate, tableName, uniqueAttribute, argument, rowMapper);

        if(results.isEmpty()) {
            return null;
        } else if(results.size() > 1) {
            LOGGER.error(tableName + " list returned for id " + uniqueAttribute + " is " + results.size() + "!");
            return null;
        } else {
            return results.get(0);
        }
    }

    public static <E> List<E> findAllByAttribute(JdbcTemplate jdbcTemplate, String tableName, String attribute, Object argument, RowMapper<E> rowMapper) {
        String sql = "SELECT * FROM " + tableName + " WHERE " + attribute + " = ?";

        List<E> results = jdbcTemplate.query(
                sql,
                new Object[] { argument },
                rowMapper);

        System.out.println("SQL QUERY: " + sql);

        return results;
    }

    public static <E> List<E> findAll(JdbcTemplate jdbcTemplate, String tableName, RowMapper<E> rowMapper) {
         String sql = "SELECT * FROM " + tableName;

        List<E> results = jdbcTemplate.query(sql, rowMapper);

        System.out.println("SQL QUERY: " + sql);

        return results;
    }

    public static <E> int update(JdbcTemplate jdbcTemplate, String tableName,
                                     String idAttributeName, long id, String[] attributes,
                                     Object[] params, int[] paramTypes,
                                        RowMapper<E> rowMapper) {

        if(findByUniqueAttribute(jdbcTemplate, tableName, idAttributeName, id, rowMapper) == null) {
            return 0;
        }

        String sql = "UPDATE " + tableName + " SET ";

        for(int i = 0; i < attributes.length; i++) {
            sql += attributes[i] + " = ?";
            if(i != (attributes.length - 1)) {
                sql += ", ";
            }
        }

        sql += " WHERE " + idAttributeName + " = " + id;

        int num_rows_affected = jdbcTemplate.update(sql, params, paramTypes);

        System.out.println("SQL QUERY: " + sql);
        System.out.println("Num rows affected: " + num_rows_affected);

        return num_rows_affected;
    }

}
