package com.ministore.ministore.repositories;

import com.ministore.ministore.model.Payment;
import com.ministore.ministore.repositories.rowmappers.PaymentRowMapper;
import com.ministore.ministore.repositories.rowmappers.PaymentRowMapper.PaymentAttribute;
import com.ministore.ministore.repositories.util.RepositoryCommons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 12/3/2017.
 */
@Repository
public class PaymentRepository extends AbstractRepository<Payment> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String TABLE_NAME = "Payment";

    public List<Payment> findAllByCustomerId(long customerId) {
        return RepositoryCommons.findAllByAttribute(jdbcTemplate,
                getTableName(),
                PaymentAttribute.customerId.toString(),
                customerId,
                getRowMapper());
    }

    @Override
    public Number save(Payment payment) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName(TABLE_NAME);
        jdbcInsert.usingGeneratedKeyColumns(PaymentAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(PaymentAttribute.creditCardNumber.toString(), payment.getCreditCard().getCreditCardNumber());
        params.put(PaymentAttribute.expirationDate.toString(), payment.getCreditCard().getExpirationDate());
        params.put(PaymentAttribute.customerId.toString(), payment.getCustomerId());

        final Number key =  jdbcInsert.executeAndReturnKey(params);

        payment.setId(key.longValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return PaymentAttribute.id.toString();
    }

    @Override
    RowMapper<Payment> getRowMapper() {
        return new PaymentRowMapper();
    }

}
