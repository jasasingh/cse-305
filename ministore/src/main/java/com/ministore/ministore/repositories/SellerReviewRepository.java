package com.ministore.ministore.repositories;

import com.ministore.ministore.model.SellerReview;
import com.ministore.ministore.repositories.rowmappers.SellerReviewRowMapper;
import com.ministore.ministore.repositories.rowmappers.SellerReviewRowMapper.SellerReviewAttribute;
import com.ministore.ministore.repositories.util.RepositoryCommons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 12/3/2017.
 */
@Repository
public class SellerReviewRepository extends AbstractRepository<SellerReview> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String TABLE_NAME = "SellerReview";

    public List<SellerReview> findAllByCustomerId(long customerId) {
        return RepositoryCommons.findAllByAttribute(jdbcTemplate,
                getTableName(),
                SellerReviewAttribute.customerId.toString(),
                customerId,
                getRowMapper());
    }

    @Override
    public Number save(SellerReview sellerReview) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName(TABLE_NAME);
        jdbcInsert.usingGeneratedKeyColumns(SellerReviewAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(SellerReviewAttribute.rating.toString(), sellerReview.getRating());
        params.put(SellerReviewAttribute.detailedReview.toString(), sellerReview.getDetailedReview());
        params.put(SellerReviewAttribute.customerId.toString(), sellerReview.getCustomerId());
        params.put(SellerReviewAttribute.sellerId.toString(), sellerReview.getSellerId());

        final Number key = jdbcInsert.executeAndReturnKey(params);

        sellerReview.setId(key.longValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return SellerReviewAttribute.id.toString();
    }

    @Override
    RowMapper<SellerReview> getRowMapper() {
        return new SellerReviewRowMapper();
    }

}
