package com.ministore.ministore.repositories;

import com.ministore.ministore.repositories.util.RepositoryCommons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.Types;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public abstract class AbstractRepository<E> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public E findById(long id) {
        return RepositoryCommons.findByUniqueAttribute(jdbcTemplate,
                getTableName(),
                getIdAttributeName(),
                id,
                getRowMapper());
    }

    public boolean delete(long id) {
        if(findById(id) == null) {
            return false;
        }

        String sql = "DELETE FROM " + getTableName() + " WHERE " + getIdAttributeName() + " = ?";

        Object[] params = { id };
        int[] types = {Types.BIGINT };

        int num_rows_affected = jdbcTemplate.update(sql, params, types);

        System.out.println("SQL QUERY: " + sql);

        return num_rows_affected == 1;
    }

    public abstract Number save(E obj);

    abstract String getTableName();

    abstract String getIdAttributeName();

    abstract RowMapper<E> getRowMapper();
}
