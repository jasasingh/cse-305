package com.ministore.ministore.repositories.rowmappers;

import com.ministore.ministore.model.Customer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.ministore.ministore.repositories.rowmappers.CustomerRowMapper.CustomerAttribute.*;

/**
 * Created by Jaspreet on 12/6/2017.
 */
public class CustomerRowMapper implements RowMapper<Customer> {

    @Override
    public Customer mapRow(ResultSet resultSet, int i) throws SQLException {
        Customer customer = new Customer();

        customer.setId(resultSet.getLong(id.toString()))
                .setEmailId(resultSet.getString(email.toString()))
                .setPassword(resultSet.getString(password.toString()));

        customer.getPerson()
                .setFirstName(resultSet.getString(firstName.toString()))
                .setLastName(resultSet.getString(lastName.toString()))
                .setPhoneNumber(resultSet.getString(telephone.toString()))
                .getAddress()
                .setStreetAddress(resultSet.getString(streetAddress.toString()))
                .setCity(resultSet.getString(city.toString()))
                .setState(resultSet.getString(state.toString()))
                .setZipCode(resultSet.getString(zipCode.toString()));

        return customer;
    }

    public enum CustomerAttribute implements Attributes {
        id ("id"),
        email("email_id"),
        password("pswd"),
        firstName("first_name"),
        lastName("last_name"),
        streetAddress("street_address"),
        city("city"),
        state("state"),
        zipCode("zipcode"),
        telephone("phone_number");

        private final String name;

        private static List<String> attributes;

        private CustomerAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(CustomerAttribute attribute : CustomerAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }

}

