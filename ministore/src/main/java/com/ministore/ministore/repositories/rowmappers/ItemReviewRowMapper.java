package com.ministore.ministore.repositories.rowmappers;

import com.ministore.ministore.model.ItemReview;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.ministore.ministore.repositories.rowmappers.ItemReviewRowMapper.ItemReviewAttribute.*;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class ItemReviewRowMapper implements RowMapper<ItemReview> {

    @Override
    public ItemReview mapRow(ResultSet resultSet, int i) throws SQLException {
        ItemReview itemReview = new ItemReview();
        itemReview.setCustomerId(resultSet.getLong(id.toString()))
                .setRating(resultSet.getShort(rating.toString()))
                .setDetailedReview(resultSet.getString(detailedReview.toString()))
                .setItemId(resultSet.getLong(itemId.toString()))
                .setCustomerId(resultSet.getLong(customerId.toString()));
        return itemReview;
    }

    public enum ItemReviewAttribute implements Attributes {
        id ("id"),
        rating("rating"),
        detailedReview("detailed_review"),
        itemId("item_id"),
        customerId("customer_id");

        private final String name;

        private static List<String> attributes;

        private ItemReviewAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(ItemReviewAttribute attribute : ItemReviewAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }
    
}
