package com.ministore.ministore.repositories;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class ViewQueries {

    public static final String items_display_sql =
            "SELECT InventoryItem.id AS id, " +
            "InventoryItem.price AS price, " +
            "InventoryItem.quantity AS quantity, " +
            "Item.item_name AS name, " +
            "Item.description AS description, " +
            "Seller.company_name AS seller_name, " +
            "Seller.id AS seller_id " +
                    "FROM InventoryItem " +
                    "JOIN Item ON InventoryItem.item_id = Item.id " +
                    "JOIN Seller ON InventoryItem.seller_id = Seller.id";
    public static final RowMapper<HashMap<String, Object>> item_display_sql_row_mapper = new RowMapper<HashMap<String, Object>>() {
        @Override
        public HashMap<String, Object> mapRow(ResultSet resultSet, int i) throws SQLException {
            HashMap<String, Object> map = new HashMap<>();

            map.put("id", resultSet.getLong("id"));
            map.put("price", resultSet.getDouble("price"));
            map.put("quantity", resultSet.getInt("quantity"));
            map.put("name", resultSet.getString("name"));
            map.put("description", resultSet.getString("description"));
            map.put("seller_name", resultSet.getString("seller_name"));
            map.put("seller_id", resultSet.getLong("seller_id"));

            return map;
        }
    };

    public static final String items_display_sql_with_search =
            "SELECT InventoryItem.id AS id, " +
                    "InventoryItem.price AS price, " +
                    "InventoryItem.quantity AS quantity, " +
                    "Item.item_name AS name, " +
                    "Item.description AS description, " +
                    "Seller.company_name AS seller_name, " +
                    "Seller.id AS seller_id " +
                    "FROM InventoryItem " +
                    "JOIN Item ON InventoryItem.item_id = Item.id " +
                    "JOIN Seller ON InventoryItem.seller_id = Seller.id " +
                    "WHERE ( name LIKE ? OR description LIKE ? )";

    public static final String shopping_cart_sql =
            "SELECT ShoppingCart.id AS id, " +
                "InventoryItem.price AS price, " +
                "InventoryItem.quantity AS inventory_quantity, " +
                "Item.item_name AS name, " +
                "Item.description AS description, " +
                "Seller.company_name AS seller_name, " +
                "Seller.id AS seller_id, " +
                "ShoppingCart.quantity AS quantity " +
                    "FROM (SELECT * FROM ShoppingCartItem WHERE customer_id = ?) AS ShoppingCart " +
                    "JOIN InventoryItem ON ShoppingCart.inventory_item_id = InventoryItem.id " +
                    "JOIN Item ON InventoryItem.item_id = Item.id " +
                    "JOIN Seller ON InventoryItem.seller_id = Seller.id";
    public static final RowMapper<HashMap<String, Object>> shopping_cart_sql_row_mapper = new RowMapper<HashMap<String, Object>>() {
        @Override
        public HashMap<String, Object> mapRow(ResultSet resultSet, int i) throws SQLException {
            HashMap<String, Object> map = new HashMap<>();

            map.put("id", resultSet.getLong("id"));
            map.put("price", resultSet.getDouble("price"));
            map.put("inventory_quantity", resultSet.getInt("inventory_quantity"));
            map.put("quantity", resultSet.getInt("quantity"));
            map.put("name", resultSet.getString("name"));
            map.put("description", resultSet.getString("description"));
            map.put("seller_name", resultSet.getString("seller_name"));
            map.put("seller_id", resultSet.getLong("seller_id"));

            return map;
        }
    };
}
