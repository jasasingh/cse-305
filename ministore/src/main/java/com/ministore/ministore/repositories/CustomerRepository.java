package com.ministore.ministore.repositories;

import com.ministore.ministore.model.Customer;
import com.ministore.ministore.repositories.util.RepositoryCommons;
import com.ministore.ministore.repositories.util.SingletonSQLConnection;
import com.ministore.ministore.repositories.rowmappers.CustomerRowMapper;
import com.ministore.ministore.repositories.rowmappers.CustomerRowMapper.CustomerAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;


import java.sql.PreparedStatement;
import java.util.HashMap;

@Repository
public class CustomerRepository extends AbstractRepository<Customer> {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    public PreparedStatement customerInsert;
    public PreparedStatement next_shopping_cart_id;
    public SingletonSQLConnection connection;

    public static final String TABLE_NAME = "Customer";

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerRepository.class);

    public Customer findByEmailId(String emailId) {
        return RepositoryCommons.findByUniqueAttribute(jdbcTemplate,
                TABLE_NAME,
                CustomerAttribute.email.toString(),
                emailId,
                new CustomerRowMapper());
    }

    @Override
    public Number save(Customer customer) {
        Customer c = findByEmailId(customer.getEmailId());
        if(c != null) {
            return null;
        }
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName(TABLE_NAME);
        jdbcInsert.usingGeneratedKeyColumns(CustomerAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(CustomerAttribute.firstName.toString(), customer.getPerson().getFirstName());
        params.put(CustomerAttribute.lastName.toString(), customer.getPerson().getLastName());
        params.put(CustomerAttribute.telephone.toString(), customer.getPerson().getPhoneNumber());
        params.put(CustomerAttribute.streetAddress.toString(), customer.getPerson().getAddress().getStreetAddress());
        params.put(CustomerAttribute.city.toString(), customer.getPerson().getAddress().getCity());
        params.put(CustomerAttribute.state.toString(), customer.getPerson().getAddress().getState());
        params.put(CustomerAttribute.zipCode.toString(), customer.getPerson().getAddress().getZipCode());
        params.put(CustomerAttribute.email.toString(), customer.getEmailId());
        params.put(CustomerAttribute.password.toString(), customer.getPassword());

        final Number key = jdbcInsert.executeAndReturnKey(params);

        customer.setId(key.longValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return CustomerAttribute.id.toString();
    }

    @Override
    RowMapper<Customer> getRowMapper() {
        return new CustomerRowMapper();
    }

}
