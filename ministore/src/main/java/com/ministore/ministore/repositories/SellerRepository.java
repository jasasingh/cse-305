package com.ministore.ministore.repositories;

import com.ministore.ministore.model.Seller;
import com.ministore.ministore.repositories.rowmappers.SellerRowMapper;
import com.ministore.ministore.repositories.rowmappers.SellerRowMapper.SellerAttribute;
import com.ministore.ministore.repositories.util.RepositoryCommons;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;


import java.util.HashMap;

/**
 * Created by Jaspreet on 12/3/2017.
 */
@Repository
public class SellerRepository extends AbstractRepository<Seller> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(SellerRepository.class);

    public static final String TABLE_NAME = "Seller";

    public Seller findByEmailId(String emailId) {
        return RepositoryCommons.findByUniqueAttribute(jdbcTemplate,
                TABLE_NAME,
                SellerAttribute.email.toString(),
                emailId,
                new SellerRowMapper());
    }

    @Override
    public Number save(Seller seller) {
        if(findByEmailId(seller.getEmailId()) != null) {
            System.out.println("An attempt to save a duplicate Seller has been thwarted.");
            return new Long(-1);
        }

        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName(TABLE_NAME);
        jdbcInsert.usingGeneratedKeyColumns(SellerAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(SellerAttribute.companyName.toString(), seller.getCompanyName());
        params.put(SellerAttribute.email.toString(), seller.getEmailId());
        params.put(SellerAttribute.password.toString(), seller.getPassword());
        params.put(SellerAttribute.streetAddress.toString(), seller.getAddress().getStreetAddress());
        params.put(SellerAttribute.city.toString(), seller.getAddress().getCity());
        params.put(SellerAttribute.state.toString(), seller.getAddress().getState());
        params.put(SellerAttribute.zipCode.toString(), seller.getAddress().getZipCode());
        params.put(SellerAttribute.telephone.toString(), seller.getPhoneNumber());

        final Number key = jdbcInsert.executeAndReturnKey(params);

        seller.setId(key.longValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return SellerAttribute.id.toString();
    }

    @Override
    RowMapper<Seller> getRowMapper() {
        return new SellerRowMapper();
    }

}
