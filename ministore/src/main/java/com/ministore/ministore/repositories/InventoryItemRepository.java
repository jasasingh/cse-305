package com.ministore.ministore.repositories;

import com.ministore.ministore.model.InventoryItem;
import com.ministore.ministore.repositories.rowmappers.InventoryItemRowMapper;
import com.ministore.ministore.repositories.rowmappers.InventoryItemRowMapper.InventoryItemAttribute;
import com.ministore.ministore.repositories.util.RepositoryCommons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 12/3/2017.
 */
@Repository
public class InventoryItemRepository extends AbstractRepository<InventoryItem> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String TABLE_NAME = "InventoryItem";

    public List<HashMap<String, Object>> findAll_view() {
        return jdbcTemplate.query(ViewQueries.items_display_sql, ViewQueries.item_display_sql_row_mapper);
    }

    public List<HashMap<String, Object>> findAllRelevant_view(String search) {
        String relevantSearch = "%" + search + "%";
        return jdbcTemplate.query(ViewQueries.items_display_sql,
                new Object[] { search, search },
                ViewQueries.item_display_sql_row_mapper);
    }

    public void updateOrderItemQuantity(long inventoryItemId, int newQuantity) {
        RepositoryCommons.update(
                jdbcTemplate,
                getTableName(),
                getIdAttributeName(),
                inventoryItemId,
                new String[] { InventoryItemAttribute.quantity.toString() },
                new Object[] { newQuantity },
                new int[] { Types.INTEGER },
                getRowMapper()
        );
    }

    @Override
    public Number save(InventoryItem inventoryItem) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName(TABLE_NAME);
        jdbcInsert.usingGeneratedKeyColumns(InventoryItemAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(InventoryItemAttribute.price.toString(), inventoryItem.getPrice());
        params.put(InventoryItemAttribute.quantity.toString(), inventoryItem.getQuantity());
        params.put(InventoryItemAttribute.itemId.toString(), inventoryItem.getItemId());
        params.put(InventoryItemAttribute.sellerId.toString(), inventoryItem.getSellerId());

        final Number key = jdbcInsert.execute(params);

        inventoryItem.setId(key.longValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return InventoryItemAttribute.id.toString();
    }

    @Override
    RowMapper<InventoryItem> getRowMapper() {
        return new InventoryItemRowMapper();
    }

}
