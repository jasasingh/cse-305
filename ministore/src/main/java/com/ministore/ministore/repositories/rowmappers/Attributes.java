package com.ministore.ministore.repositories.rowmappers;

import java.util.List;

/**
 * Created by Jaspreet on 12/7/2017.
 */
public interface Attributes {

    List<String> getAttributes();

}
