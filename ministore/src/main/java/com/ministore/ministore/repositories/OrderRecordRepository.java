package com.ministore.ministore.repositories;

import com.ministore.ministore.model.OrderRecord;
import com.ministore.ministore.repositories.rowmappers.OrderRecordRowMapper;
import com.ministore.ministore.repositories.rowmappers.OrderRecordRowMapper.OrderRecordAttribute;
import com.ministore.ministore.repositories.util.RepositoryCommons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 12/3/2017.
 */
@Repository
public class OrderRecordRepository extends AbstractRepository<OrderRecord> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String TABLE_NAME = "OrderRecord";

    public List<OrderRecord> findByCustomerId(long customerId) {
        return RepositoryCommons.findAllByAttribute(
                jdbcTemplate,
                getTableName(),
                OrderRecordAttribute.customerId.toString(),
                customerId,
                getRowMapper()
        );
    }

    @Override
    public Number save(OrderRecord orderRecord) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName(TABLE_NAME);
        jdbcInsert.usingGeneratedKeyColumns(OrderRecordAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(OrderRecordAttribute.streetAddress.toString(), orderRecord.getAddress().getStreetAddress());
        params.put(OrderRecordAttribute.city.toString(), orderRecord.getAddress().getCity());
        params.put(OrderRecordAttribute.state.toString(), orderRecord.getAddress().getState());
        params.put(OrderRecordAttribute.zipCode.toString(), orderRecord.getAddress().getZipCode());
        params.put(OrderRecordAttribute.customerId.toString(), orderRecord.getCustomerId());
        params.put(OrderRecordAttribute.cardInfo.toString(), orderRecord.getCardInfo());
        params.put(OrderRecordAttribute.orderDate.toString(), orderRecord.getOrderDate());
        params.put(OrderRecordAttribute.shippingFee.toString(), orderRecord.getShippingFee());
        params.put(OrderRecordAttribute.tax.toString(), orderRecord.getTax());

        final Number key = jdbcInsert.executeAndReturnKey(params);

        orderRecord.setId(key.longValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return OrderRecordAttribute.id.toString();
    }

    @Override
    RowMapper<OrderRecord> getRowMapper() {
        return new OrderRecordRowMapper();
    }


}
