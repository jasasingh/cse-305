package com.ministore.ministore.repositories.util;
import org.springframework.web.context.annotation.ApplicationScope;
import com.ministore.ministore.repositories.rowmappers.CustomerRowMapper;


import java.sql.*;

@ApplicationScope
public class SingletonSQLConnection {
    private static SingletonSQLConnection ourInstance = new SingletonSQLConnection();

    public static long userCount = 0;

    public static int getTempKeyGen() {
        return tempKeyGen;
    }

    public static int tempKeyGen = 404;
    public static Connection connection;


    public static PreparedStatement customerInsert;

    public static PreparedStatement getShopping_cart_id() {
        try {
            return  connection.prepareStatement("SELECT MAX(id) FROM Customer");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;   }

    public static PreparedStatement shopping_cart;


    public synchronized static SingletonSQLConnection getInstance() {
        tempKeyGen++;

        userCount++;

        return ourInstance;
    }

    public SingletonSQLConnection() {

        initializeJdbc();
    }


    private synchronized  void initializeJdbc(){

        try{


            Class.forName("com.mysql.jdbc.Driver");

            String username= "youroljas";
            String password="T3MmeD0#4rbmunyV";
            String myUrl =  "jdbc:mysql://localhost/cse305trip?autoReconnect=true&useSSL?=false";
            String url = "jdbc:mysql://cse305.cpvim3cth9tj.us-east-2.rds.amazonaws.com/cse305trip?autoReconnect=true";

          //  connection =DriverManager.getConnection(myUrl,"root","password"); //mylocal
            connection =DriverManager.getConnection(url,username,password);


            String sqlStatement= new InsertStatementBuilder("Customer")
                .addAttributes(CustomerRowMapper.CustomerAttribute.id.getAttributes())
                .build();

              customerInsert = connection.prepareStatement(sqlStatement);



        }catch(Exception e){
            System.out.println("JDBC>>>>: "+e.getMessage());
            e.printStackTrace();


        }
    }


    public synchronized static PreparedStatement getCustomerInsert() {
        return customerInsert;
    }


    /**
     * Syncronize method since each session has a copy of the connection
     * Frees one connection if it is the last connection, it closes the connection
     */

    public synchronized static void free() {
        userCount--;

        if(userCount <= 0){
            userCount = 0;

            try {
                customerInsert.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }


        }
        else{
            userCount--;
        }





    }

    /**
     * Syncronize method since each session has a copy of the connection
     * @param customerInsert
     */

    public synchronized void executeUpdate(PreparedStatement customerInsert) {


        try {
            customerInsert.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
