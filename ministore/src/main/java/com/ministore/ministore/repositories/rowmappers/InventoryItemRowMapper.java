package com.ministore.ministore.repositories.rowmappers;

import com.ministore.ministore.model.InventoryItem;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.ministore.ministore.repositories.rowmappers.InventoryItemRowMapper.InventoryItemAttribute.*;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class InventoryItemRowMapper implements RowMapper<InventoryItem> {

    @Override
    public InventoryItem mapRow(ResultSet resultSet, int i) throws SQLException {
        InventoryItem inventoryItem = new InventoryItem();

        inventoryItem.setId(resultSet.getLong(id.toString()))
                .setPrice(resultSet.getDouble(price.toString()))
                .setQuantity(resultSet.getInt(quantity.toString()))
                .setItemId(resultSet.getLong(itemId.toString()))
                .setSellerId(resultSet.getLong(sellerId.toString()));

        return inventoryItem;
    }

    public enum InventoryItemAttribute implements Attributes{
        id ("id"),
        price("price"),
        quantity("quantity"),
        itemId("item_id"),
        sellerId("seller_id");

        private final String name;

        private static List<String> attributes;

        private InventoryItemAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(InventoryItemAttribute attribute : InventoryItemAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }
}
