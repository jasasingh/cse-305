package com.ministore.ministore.repositories.rowmappers;

import com.ministore.ministore.model.OrderRecord;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.ministore.ministore.repositories.rowmappers.OrderRecordRowMapper.OrderRecordAttribute.*;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class OrderRecordRowMapper implements RowMapper<OrderRecord> {

    @Override
    public OrderRecord mapRow(ResultSet resultSet, int i) throws SQLException {
        OrderRecord orderRecord = new OrderRecord();

        orderRecord.setId(resultSet.getLong(id.toString()))
                .setCustomerId(resultSet.getLong(customerId.toString()))
                .setOrderDate(resultSet.getDate(orderDate.toString()))
                .setShippingFee(resultSet.getDouble(shippingFee.toString()))
                .setTax(resultSet.getDouble(tax.toString()))
                .setCardInfo(resultSet.getString(cardInfo.toString()))
                .getAddress()
                .setStreetAddress(resultSet.getString(streetAddress.toString()))
                .setCity(resultSet.getString(city.toString()))
                .setState(resultSet.getString(state.toString()))
                .setZipCode(resultSet.getString(zipCode.toString()));

        return orderRecord;
    }

    public enum OrderRecordAttribute implements Attributes{
        id ("id"),
        orderDate("order_date"),
        streetAddress("street_address"),
        city("city"),
        state("state"),
        zipCode("zipcode"),
        shippingFee("shipping_fee"),
        tax("tax"),
        cardInfo("card_info"),
        customerId("customer_id");

        private final String name;

        private static List<String> attributes;

        private OrderRecordAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(OrderRecordAttribute attribute : OrderRecordAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }

}
