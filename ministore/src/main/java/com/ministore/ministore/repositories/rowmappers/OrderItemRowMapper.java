package com.ministore.ministore.repositories.rowmappers;

import com.ministore.ministore.model.OrderItem;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.ministore.ministore.repositories.rowmappers.OrderItemRowMapper.OrderItemAttribute.*;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class OrderItemRowMapper implements RowMapper<OrderItem> {


    @Override
    public OrderItem mapRow(ResultSet resultSet, int i) throws SQLException {
        OrderItem orderItem = new OrderItem();
        orderItem.setId(resultSet.getLong(id.toString()))
                .setSellerName(resultSet.getString(sellerName.toString()))
                .setPrice(resultSet.getDouble(price.toString()))
                .setQuantity(resultSet.getInt(quantity.toString()))
                .setItemId(resultSet.getLong(itemId.toString()))
                .setOrderRecordId(resultSet.getLong(orderRecordId.toString()));
        return orderItem;
    }

    public enum OrderItemAttribute implements Attributes{
        id ("id"),
        sellerName("seller_name"),
        price("price"),
        quantity("quantity"),
        itemId("item_id"),
        orderRecordId("order_record_id");

        private final String name;

        private static List<String> attributes;

        private OrderItemAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(OrderItemAttribute attribute : OrderItemAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }
}
