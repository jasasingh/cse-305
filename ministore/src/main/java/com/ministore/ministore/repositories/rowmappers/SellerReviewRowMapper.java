package com.ministore.ministore.repositories.rowmappers;

import com.ministore.ministore.model.SellerReview;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.ministore.ministore.repositories.rowmappers.SellerReviewRowMapper.SellerReviewAttribute.*;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class SellerReviewRowMapper implements RowMapper<SellerReview> {

    @Override
    public SellerReview mapRow(ResultSet resultSet, int i) throws SQLException {
        SellerReview sellerReview = new SellerReview();

        sellerReview.setId(resultSet.getLong(id.toString()))
                .setRating(resultSet.getShort(rating.toString()))
                .setDetailedReview(resultSet.getString(detailedReview.toString()))
                .setCustomerId(resultSet.getLong(customerId.toString()))
                .setSellerId(resultSet.getLong(sellerId.toString()));

        return sellerReview;
    }

    public enum SellerReviewAttribute implements Attributes{
        id ("id"),
        rating("rating"),
        detailedReview("detailed_review"),
        customerId("customer_id"),
        sellerId("seller_id");

        private final String name;

        private static List<String> attributes;

        private SellerReviewAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(SellerReviewAttribute attribute : SellerReviewAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }

}
