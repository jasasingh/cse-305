package com.ministore.ministore.repositories;

import com.ministore.ministore.model.ItemReview;
import com.ministore.ministore.repositories.rowmappers.ItemReviewRowMapper;
import com.ministore.ministore.repositories.rowmappers.ItemReviewRowMapper.ItemReviewAttribute;
import com.ministore.ministore.repositories.util.RepositoryCommons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 12/3/2017.
 */
@Repository
public class ItemReviewRepository extends AbstractRepository<ItemReview> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String TABLE_NAME = "ItemReview";

    public List<ItemReview> findAllByCustomerId(long customerId) {
        return RepositoryCommons.findAllByAttribute(jdbcTemplate,
                getTableName(),
                ItemReviewAttribute.customerId.toString(),
                customerId,
                getRowMapper());
    }

    @Override
    public Number save(ItemReview itemReview) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName(TABLE_NAME);
        jdbcInsert.usingGeneratedKeyColumns(ItemReviewAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(ItemReviewAttribute.rating.toString(), itemReview.getRating());
        params.put(ItemReviewAttribute.detailedReview.toString(), itemReview.getDetailedReview());
        params.put(ItemReviewAttribute.itemId.toString(), itemReview.getItemId());
        params.put(ItemReviewAttribute.customerId.toString(), itemReview.getCustomerId());

        final Number key = jdbcInsert.executeAndReturnKey(params);

        itemReview.setId(key.longValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return ItemReviewAttribute.id.toString();
    }

    @Override
    RowMapper<ItemReview> getRowMapper() {
        return new ItemReviewRowMapper();
    }

}
