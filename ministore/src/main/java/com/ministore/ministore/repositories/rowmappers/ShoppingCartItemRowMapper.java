package com.ministore.ministore.repositories.rowmappers;

import com.ministore.ministore.model.ShoppingCartItem;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.ministore.ministore.repositories.rowmappers.ShoppingCartItemRowMapper.ShoppingCartItemAttribute.*;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class ShoppingCartItemRowMapper implements RowMapper<ShoppingCartItem> {

    @Override
    public ShoppingCartItem mapRow(ResultSet resultSet, int i) throws SQLException {
        ShoppingCartItem shoppingCartItem = new ShoppingCartItem();

        shoppingCartItem.setId(resultSet.getLong(id.toString()))
                .setInventoryItemId(resultSet.getLong(inventoryItemId.toString()))
                .setCustomerId(resultSet.getLong(customerId.toString()))
                .setQuantity(resultSet.getInt(quantity.toString()));

        return shoppingCartItem;
    }

    public enum ShoppingCartItemAttribute implements Attributes{
        id ("id"),
        customerId("customer_id"),
        inventoryItemId("inventory_item_id"),
        quantity("quantity");

        private final String name;

        private static List<String> attributes;

        private ShoppingCartItemAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(ShoppingCartItemAttribute attribute : ShoppingCartItemAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }

}
