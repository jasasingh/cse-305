package com.ministore.ministore.repositories;

import com.ministore.ministore.model.Item;
import com.ministore.ministore.repositories.rowmappers.ItemRowMapper;
import com.ministore.ministore.repositories.rowmappers.ItemRowMapper.ItemAttribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

/**
 * Created by Jaspreet on 12/3/2017.
 */
@Repository
public class ItemRepository extends AbstractRepository<Item> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String TABLE_NAME = "Item";

    @Override
    public Number save(Item item) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName(TABLE_NAME);
        jdbcInsert.usingGeneratedKeyColumns(ItemAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(ItemAttribute.itemName.toString(), item.getItemName());
        params.put(ItemAttribute.description.toString(), item.getDescription());

        final Number key = jdbcInsert.executeAndReturnKey(params);

        item.setId(key.longValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return ItemAttribute.id.toString();
    }

    @Override
    RowMapper<Item> getRowMapper() {
        return new ItemRowMapper();
    }

}
