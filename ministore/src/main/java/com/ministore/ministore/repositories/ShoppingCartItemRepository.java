package com.ministore.ministore.repositories;

import com.ministore.ministore.model.ShoppingCartItem;
import com.ministore.ministore.repositories.rowmappers.ShoppingCartItemRowMapper;
import com.ministore.ministore.repositories.rowmappers.ShoppingCartItemRowMapper.ShoppingCartItemAttribute;
import com.ministore.ministore.repositories.util.RepositoryCommons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 12/3/2017.
 */
@Repository
public class ShoppingCartItemRepository extends AbstractRepository<ShoppingCartItem> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String TABLE_NAME = "ShoppingCartItem";

    public List<HashMap<String, Object>> findAllByCustomerId_view(long customerId) {
        return jdbcTemplate.query(ViewQueries.shopping_cart_sql,
                new Object[] { customerId },
                ViewQueries.shopping_cart_sql_row_mapper);
    }

    public List<ShoppingCartItem> findAllByCustomerId(long customerId) {
        return RepositoryCommons.findAllByAttribute(
                jdbcTemplate,
                getTableName(),
                ShoppingCartItemAttribute.customerId.toString(),
                customerId,
                getRowMapper()
        );
    }

    public void updateShoppingCartItemQuantity(long shoppingCartId, int newQuantity) {
        RepositoryCommons.update(
                jdbcTemplate,
                getTableName(),
                getIdAttributeName(),
                shoppingCartId,
                new String[] { ShoppingCartItemAttribute.quantity.toString() },
                new Object[] { newQuantity },
                new int[] { Types.INTEGER },
                getRowMapper()
        );
    }

    @Override
    public Number save(ShoppingCartItem shoppingCartItem) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName(TABLE_NAME);
        jdbcInsert.usingGeneratedKeyColumns(ShoppingCartItemAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(ShoppingCartItemAttribute.inventoryItemId.toString(), shoppingCartItem.getInventoryItemId());
        params.put(ShoppingCartItemAttribute.customerId.toString(), shoppingCartItem.getCustomerId());
        params.put(ShoppingCartItemAttribute.quantity.toString(), shoppingCartItem.getQuantity());

        final Number key = jdbcInsert.executeAndReturnKey(params);

        shoppingCartItem.setId(key.longValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return ShoppingCartItemAttribute.id.toString();
    }

    @Override
    RowMapper<ShoppingCartItem> getRowMapper() {
        return new ShoppingCartItemRowMapper();
    }

}
