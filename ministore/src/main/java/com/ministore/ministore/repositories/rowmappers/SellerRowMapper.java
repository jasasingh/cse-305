package com.ministore.ministore.repositories.rowmappers;

import com.ministore.ministore.model.Seller;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.ministore.ministore.repositories.rowmappers.SellerRowMapper.SellerAttribute.*;

/**
 * Created by Jaspreet on 12/6/2017.
 */
public class SellerRowMapper implements RowMapper<Seller> {

    @Override
    public Seller mapRow(ResultSet resultSet, int i) throws SQLException {
        Seller seller = new Seller();

        seller.setId(resultSet.getLong(id.toString()))
                .setEmailId(resultSet.getString(email.toString()))
                .setPassword(resultSet.getString(password.toString()));

        seller.setCompanyName(resultSet.getString(companyName.toString()))
                .setPhoneNumber(resultSet.getString(telephone.toString()))
                .getAddress()
                .setStreetAddress(resultSet.getString(streetAddress.toString()))
                .setCity(resultSet.getString(city.toString()))
                .setState(resultSet.getString(state.toString()))
                .setZipCode(resultSet.getString(zipCode.toString()));

        return seller;
    }

    public enum SellerAttribute implements Attributes{
        id ("id"),
        email("email_id"),
        password("pswd"),
        companyName("company_name"),
        streetAddress("street_address"),
        city("city"),
        state("state"),
        zipCode("zipcode"),
        telephone("phone_number");

        private final String name;

        private static List<String> attributes;

        private SellerAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(SellerAttribute attribute : SellerAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }
}

