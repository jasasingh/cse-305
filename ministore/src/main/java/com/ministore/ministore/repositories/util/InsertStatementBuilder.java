package com.ministore.ministore.repositories.util;

import com.ministore.ministore.repositories.rowmappers.Attributes;
import javafx.beans.binding.StringBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jaspreet on 12/7/2017.
 */
public class InsertStatementBuilder {

    private String tableName;
    private List<String> attributes;

    public InsertStatementBuilder(String tableName) {
        this.tableName = tableName;
        attributes = new ArrayList<>();
    }

    public InsertStatementBuilder addAttribute(String attribute) {
        this.attributes.add(attribute);
        return this;
    }

    public InsertStatementBuilder addAttributes(List<String> attributes) {
        for(String attribute : attributes) {
            this.attributes.add(attribute);
        }
        return this;
    }

    public String build() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO ")
                .append(tableName)
                .append(" (");

        for(int i = 0; i < attributes.size(); i++) {
            if(i != 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append(attributes.get(i));
        }
        stringBuilder.append(") VALUES (");

        for(int i = 0; i < attributes.size(); i++) {
            if(i != 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append("?");
        }
        stringBuilder.append(")");

        return stringBuilder.toString();
    }

}
