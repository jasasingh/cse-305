package com.ministore.ministore.repositories.rowmappers;

import com.ministore.ministore.model.Item;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.ministore.ministore.repositories.rowmappers.ItemRowMapper.ItemAttribute.description;
import static com.ministore.ministore.repositories.rowmappers.ItemRowMapper.ItemAttribute.id;
import static com.ministore.ministore.repositories.rowmappers.ItemRowMapper.ItemAttribute.itemName;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class ItemRowMapper implements RowMapper<Item> {

    @Override
    public Item mapRow(ResultSet resultSet, int i) throws SQLException {
        Item item = new Item();
        item.setId(resultSet.getLong(id.toString()))
                .setItemName(resultSet.getString(itemName.toString()))
                .setDescription(resultSet.getString(description.toString()));
        return item;
    }


    public enum ItemAttribute implements Attributes{
        id ("id"),
        itemName("item_name"),
        description("description");

        private final String name;

        private static List<String> attributes;

        private ItemAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(ItemAttribute attribute : ItemAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }
}
