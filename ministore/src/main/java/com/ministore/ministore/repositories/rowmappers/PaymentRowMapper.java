package com.ministore.ministore.repositories.rowmappers;

import com.ministore.ministore.model.Payment;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.ministore.ministore.repositories.rowmappers.PaymentRowMapper.PaymentAttribute.*;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class PaymentRowMapper implements RowMapper<Payment> {

    @Override
    public Payment mapRow(ResultSet resultSet, int i) throws SQLException {
        Payment payment = new Payment();

        payment.setId(resultSet.getLong(id.toString()))
                .setCustomerId(resultSet.getLong(customerId.toString()))
                .getCreditCard()
                .setCreditCardNumber(resultSet.getString(creditCardNumber.toString()))
                .setExpirationDate(resultSet.getDate(expirationDate.toString()));

        return payment;
    }

    public enum PaymentAttribute implements Attributes {
        id ("id"),
        creditCardNumber("card_number"),
        expirationDate("expiration_date"),
        customerId("customer_id");

        private final String name;

        private static List<String> attributes;

        private PaymentAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(PaymentAttribute attribute : PaymentAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }

}
