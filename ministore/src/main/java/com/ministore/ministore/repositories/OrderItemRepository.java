package com.ministore.ministore.repositories;

import com.ministore.ministore.model.OrderItem;
import com.ministore.ministore.repositories.rowmappers.OrderItemRowMapper;
import com.ministore.ministore.repositories.rowmappers.OrderItemRowMapper.OrderItemAttribute;
import com.ministore.ministore.repositories.rowmappers.OrderRecordRowMapper;
import com.ministore.ministore.repositories.util.RepositoryCommons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 12/3/2017.
 */
@Repository
public class OrderItemRepository extends AbstractRepository<OrderItem> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String TABLE_NAME = "OrderItem";

    public List<OrderItem> findAllByOrderRecordId(long orderRecordId) {
        return RepositoryCommons.findAllByAttribute(
                jdbcTemplate,
                getTableName(),
                OrderItemAttribute.orderRecordId.toString(),
                orderRecordId,
                getRowMapper()
        );
    }

    @Override
    public Number save(OrderItem orderItem) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName(TABLE_NAME);
        jdbcInsert.usingGeneratedKeyColumns(OrderItemAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(OrderItemAttribute.price.toString(), orderItem.getPrice());
        params.put(OrderItemAttribute.quantity.toString(), orderItem.getQuantity());
        params.put(OrderItemAttribute.sellerName.toString(), orderItem.getSellerName());
        params.put(OrderItemAttribute.itemId.toString(), orderItem.getItemId());
        params.put(OrderItemAttribute.orderRecordId.toString(), orderItem.getOrderRecordId());

        System.out.println(params.toString());

        final Number key = jdbcInsert.executeAndReturnKey(params);

        orderItem.setId(key.longValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return OrderItemAttribute.id.toString();
    }

    @Override
    RowMapper<OrderItem> getRowMapper() {
        return new OrderItemRowMapper();
    }

}
