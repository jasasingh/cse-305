package com.ministore.ministore.model;

import com.ministore.ministore.model.composition.Address;

/**
 * Created by Jaspreet on 12/6/2017.
 */
public class Seller extends User {

    private String companyName;

    private Address address;

    private String phoneNumber;

    public Seller() {
        super();
        this.address = new Address();
    }

    public Seller(Seller seller) {
        super(seller);
        this.companyName = seller.getCompanyName();
        this.address = seller.getAddress().clone();
        this.phoneNumber = seller.getPhoneNumber();
    }

    public String getCompanyName() {
        return companyName;
    }

    public Seller setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public Seller setAddress(Address address) {
        this.address = address;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Seller setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
}
