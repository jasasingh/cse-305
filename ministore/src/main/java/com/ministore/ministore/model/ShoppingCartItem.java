package com.ministore.ministore.model;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class ShoppingCartItem {

    private long id;

    // foreign key
    private long inventoryItemId;

    // foreign key
    private long customerId;

    private int quantity;

    public long getId() {
        return id;
    }

    public ShoppingCartItem setId(long id) {
        this.id = id;
        return this;
    }

    public long getInventoryItemId() {
        return inventoryItemId;
    }

    public ShoppingCartItem setInventoryItemId(long inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
        return this;
    }

    public long getCustomerId() {
        return customerId;
    }

    public ShoppingCartItem setCustomerId(long customerId) {
        this.customerId = customerId;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public ShoppingCartItem setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }
}
