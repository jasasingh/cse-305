package com.ministore.ministore.model;


import com.ministore.ministore.model.composition.Person;

/**
 * Created by Jaspreet on 11/25/2017.
 */
public class Customer extends User {

    private Person person;

    public Customer() {
        super();
        this.person = new Person();
    }

    public Customer(Customer customer) {
        super(customer);
        this.person = customer.getPerson().clone();
    }

    public Person getPerson() {
        return person;
    }

    public Customer setPerson(Person person) {
        this.person = person;
        return this;
    }

}
