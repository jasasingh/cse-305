package com.ministore.ministore.model;

import com.ministore.ministore.model.composition.Address;

import java.util.Date;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class OrderRecord {

    private long id;

    private Date orderDate;

    private Address address;

    private String cardInfo;

    private double shippingFee;

    private double tax;

    // foreign key
    private long customerId;

    public OrderRecord() {
        this.address = new Address();
        this.orderDate = new Date();
    }

    public long getId() {
        return id;
    }

    public OrderRecord setId(long id) {
        this.id = id;
        return this;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public OrderRecord setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public OrderRecord setAddress(Address address) {
        this.address = address;
        return this;
    }

    public double getShippingFee() {
        return shippingFee;
    }

    public OrderRecord setShippingFee(double shippingFee) {
        this.shippingFee = shippingFee;
        return this;
    }

    public double getTax() {
        return tax;
    }

    public OrderRecord setTax(double tax) {
        this.tax = tax;
        return this;
    }

    public long getCustomerId() {
        return customerId;
    }

    public OrderRecord setCustomerId(long customerId) {
        this.customerId = customerId;
        return this;
    }

    public String getCardInfo() {
        return cardInfo;
    }

    public OrderRecord setCardInfo(String cardInfo) {
        this.cardInfo = cardInfo;
        return this;
    }

    @Override
    public String toString() {
        return "[ ORDER_RECORD id: " + this.id
                + " DATE: " + orderDate.toString() + " ]";
    }
}
