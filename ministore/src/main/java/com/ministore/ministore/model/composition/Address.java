package com.ministore.ministore.model.composition;

/**
 * Created by Jaspreet on 11/25/2017.
 */
public class Address implements Cloneable {

    private String streetAddress;

    private String city;

    private String state;

    private String zipCode;

    public Address() {}

    public String getStreetAddress() {
        return streetAddress;
    }

    public Address setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Address setCity(String city) {
        this.city = city;
        return this;
    }

    public String getState() {
        return state;
    }

    public Address setState(String state) {
        this.state = state;
        return this;
    }

    public String getZipCode() {
        return zipCode;
    }

    public Address setZipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public Address clone() {
        Address address = new Address();
        address.streetAddress = this.streetAddress;
        address.city = this.city;
        address.state = this.state;
        address.zipCode = this.zipCode;
        return address;
    }

    @Override
    public String toString() {
        return "[ streetAddress: " + streetAddress
                + " city: " + city + " state: " + state
                + " zip: " + zipCode + " ]\n";
    }

}
