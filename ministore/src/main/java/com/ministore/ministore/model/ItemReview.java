package com.ministore.ministore.model;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class ItemReview {

    private long id;

    private short rating;

    private String detailedReview;

    // foreign key
    private long itemId;

    // foreign key
    private long customerId;

    public long getId() {
        return id;
    }

    public ItemReview setId(long id) {
        this.id = id;
        return this;
    }

    public short getRating() {
        return rating;
    }

    public ItemReview setRating(short rating) {
        this.rating = rating;
        return this;
    }

    public String getDetailedReview() {
        return detailedReview;
    }

    public ItemReview setDetailedReview(String detailedReview) {
        this.detailedReview = detailedReview;
        return this;
    }

    public long getItemId() {
        return itemId;
    }

    public ItemReview setItemId(long itemId) {
        this.itemId = itemId;
        return this;
    }

    public long getCustomerId() {
        return customerId;
    }

    public ItemReview setCustomerId(long customerId) {
        this.customerId = customerId;
        return this;
    }
}
