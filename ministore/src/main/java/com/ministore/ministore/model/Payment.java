package com.ministore.ministore.model;

import com.ministore.ministore.model.composition.CreditCard;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class Payment {

    private long id;

    private CreditCard creditCard;

    private long customerId;

    public Payment() {
        this.creditCard = new CreditCard();
    }

    public long getId() {
        return id;
    }

    public Payment setId(long id) {
        this.id = id;
        return this;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public Payment setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
        return this;
    }

    public long getCustomerId() {
        return customerId;
    }

    public Payment setCustomerId(long customerId) {
        this.customerId = customerId;
        return this;
    }
}
