package com.ministore.ministore.model;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class SellerReview {

    private long id;

    private short rating;

    private String detailedReview;

    // foreign key
    private long customerId;

    // foreign key
    private long sellerId;

    public long getId() {
        return id;
    }

    public SellerReview setId(long id) {
        this.id = id;
        return this;
    }

    public short getRating() {
        return rating;
    }

    public SellerReview setRating(short rating) {
        this.rating = rating;
        return this;
    }

    public String getDetailedReview() {
        return detailedReview;
    }

    public SellerReview setDetailedReview(String detailedReview) {
        this.detailedReview = detailedReview;
        return this;
    }

    public long getCustomerId() {
        return customerId;
    }

    public SellerReview setCustomerId(long customerId) {
        this.customerId = customerId;
        return this;
    }

    public long getSellerId() {
        return sellerId;
    }

    public SellerReview setSellerId(long sellerId) {
        this.sellerId = sellerId;
        return this;
    }
}
