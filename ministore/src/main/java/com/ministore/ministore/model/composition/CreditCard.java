package com.ministore.ministore.model.composition;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Jaspreet on 11/25/2017.
 */
public class CreditCard {

    private String creditCardNumber;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date expirationDate;

    public CreditCard() {
        expirationDate = new Date();
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public CreditCard setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
        return this;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public CreditCard setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

}
