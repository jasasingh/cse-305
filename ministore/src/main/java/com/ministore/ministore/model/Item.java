package com.ministore.ministore.model;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class Item {

    private long id;

    private String itemName;

    private String description;

    public long getId() {
        return id;
    }

    public Item setId(long id) {
        this.id = id;
        return this;
    }

    public String getItemName() {
        return itemName;
    }

    public Item setItemName(String itemName) {
        this.itemName = itemName;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Item setDescription(String description) {
        this.description = description;
        return this;
    }
}
