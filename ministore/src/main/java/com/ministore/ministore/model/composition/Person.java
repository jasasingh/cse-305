package com.ministore.ministore.model.composition;
import org.hibernate.validator.constraints.NotEmpty;
/**
 * Created by Jaspreet on 11/25/2017.
 */
public class Person implements Cloneable {
    @NotEmpty
    private String firstName;

    private String lastName;

    private Address address;

    private String phoneNumber;

    public Person() {
        this.address = new Address();
    }

    public String getFirstName() {
        return firstName;
    }

    public Person setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Person setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public Person setAddress(Address address) {
        this.address = address;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Person setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public Person clone() {
        Person person = new Person();
        person.firstName = this.firstName;
        person.lastName = this.lastName;
        person.address = this.address.clone();
        person.phoneNumber = this.phoneNumber;
        return person;
    }

    @Override
    public String toString() {
        return "[ firstName: " + firstName
                + " last Name: " + lastName
                + " ";
    }

}
