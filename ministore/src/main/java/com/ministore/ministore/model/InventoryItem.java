package com.ministore.ministore.model;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class InventoryItem {

    private long id;

    private double price;

    private int quantity;

    // foreign key
    private long itemId;

    // foreign key
    private long sellerId;

    public long getId() {
        return id;
    }

    public InventoryItem setId(long id) {
        this.id = id;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public InventoryItem setPrice(double price) {
        this.price = price;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public InventoryItem setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public long getItemId() {
        return itemId;
    }

    public InventoryItem setItemId(long itemId) {
        this.itemId = itemId;
        return this;
    }

    public long getSellerId() {
        return sellerId;
    }

    public InventoryItem setSellerId(long sellerId) {
        this.sellerId = sellerId;
        return this;
    }
}
