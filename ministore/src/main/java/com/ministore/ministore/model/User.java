package com.ministore.ministore.model;

public abstract class User {

    private long id;

    private String emailId;

    private String password;

    public User() {}

    public User(User user) {
        this.id = user.getId();
        this.emailId = user.getEmailId();
        this.password = user.getPassword();
    }

    public long getId() {
        return id;
    }

    public User setId(long id) {
        this.id = id;
        return this;
    }

    public String getEmailId() {
        return emailId;
    }

    public User setEmailId(String emailId) {
        this.emailId = emailId;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

}
