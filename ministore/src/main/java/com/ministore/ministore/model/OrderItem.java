package com.ministore.ministore.model;


/**
 * Created by Jaspreet on 12/8/2017.
 */
public class OrderItem {

    private long id;

    private String sellerName;

    private double price;

    private int quantity;

    // foreign key
    private long itemId;

    // foreign key
    private long orderRecordId;

    public long getId() {
        return id;
    }

    public OrderItem setId(long id) {
        this.id = id;
        return this;
    }

    public String getSellerName() {
        return sellerName;
    }

    public OrderItem setSellerName(String sellerName) {
        this.sellerName = sellerName;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public OrderItem setPrice(double price) {
        this.price = price;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public OrderItem setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public long getItemId() {
        return itemId;
    }

    public OrderItem setItemId(long itemId) {
        this.itemId = itemId;
        return this;
    }

    public long getOrderRecordId() {
        return orderRecordId;
    }

    public OrderItem setOrderRecordId(long orderRecordId) {
        this.orderRecordId = orderRecordId;
        return this;
    }
}

