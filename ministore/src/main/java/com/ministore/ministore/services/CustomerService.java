package com.ministore.ministore.services;

import com.ministore.ministore.model.*;
import com.ministore.ministore.model.composition.CreditCard;
import com.ministore.ministore.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 11/26/2017.
 */
@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private SellerReviewRepository sellerReviewRepository;
    @Autowired
    private ItemReviewRepository itemReviewRepository;
    @Autowired
    private ShoppingCartItemRepository shoppingCartItemRepository;

    @Transactional
    public Customer save(Customer customer) {
        customer.setPassword(passwordEncoder.encode(customer.getPassword()));

        if (customerRepository.save(customer) == null) {
            return null;
        } else {
//            Payment payment = new Payment();
//            payment.setCustomerId(customer.getId());
//            payment.setCreditCard(new CreditCard().setCreditCardNumber("2133295034334443").setExpirationDate(new Date()));
//            paymentRepository.save(payment);
            return customer;
        }
    }

    @Transactional(readOnly = true)
    public Customer findByEmailId(String emailId) {
        return customerRepository.findByEmailId(emailId);
    }

    @Transactional(readOnly = true)
    public List<Payment> getAllPaymentMethods(Customer customer) {
        List<Payment> paymentMethods = paymentRepository.findAllByCustomerId(customer.getId());
        return (paymentMethods == null) ? new ArrayList<>() : paymentMethods;
    }

    @Transactional(readOnly = true)
    public List<SellerReview> getAllSellerReviews(Customer customer) {
        List<SellerReview> sellerReviews = sellerReviewRepository.findAllByCustomerId(customer.getId());
        return (sellerReviews == null) ? new ArrayList<>() : sellerReviews;
    }

    @Transactional(readOnly = true)
    public List<ItemReview> getAllItemReviews(Customer customer) {
        List<ItemReview> itemReviews = itemReviewRepository.findAllByCustomerId(customer.getId());
        return (itemReviews == null) ? new ArrayList<>() : itemReviews;
    }

    @Transactional(readOnly = true)
    public List<OrderRecord> getAllOrderRecords(Customer customer) {
        // TODO
        return new ArrayList<>();
    }

    public int addPayment(Customer customer, Payment payment) {
        long customerId = customerRepository.findByEmailId(customer.getEmailId()).getId();
        payment.setCustomerId(customerId);
        paymentRepository.save(payment);
        return 0;
    }

}
