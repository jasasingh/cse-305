package com.ministore.ministore.services;

import com.ministore.ministore.model.*;
import com.ministore.ministore.repositories.InventoryItemRepository;
import com.ministore.ministore.repositories.OrderItemRepository;
import com.ministore.ministore.repositories.OrderRecordRepository;
import com.ministore.ministore.repositories.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Jaspreet on 12/8/2017.
 */
@Service
public class OrderService {

    @Autowired
    private OrderRecordRepository orderRecordRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;


    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @Autowired
    private SellerRepository sellerRepository;

    @Autowired
    private ShoppingCartItemService shoppingCartItemService;

    private static final double SHIPPING_RATE = 0.06;
    private static final double TAX_RATE = 0.0875;

    public List<OrderItem> getAllOrderItems(Customer customer, long orderRecordId) {
        List<OrderItem> orderItems = orderItemRepository.findAllByOrderRecordId(orderRecordId);
        return orderItems == null ? new ArrayList<>() : orderItems;
    }

    public List<OrderRecord> getAllRecords(Customer customer) {
        List<OrderRecord> orderRecords = orderRecordRepository.findByCustomerId(customer.getId());
        return orderRecords == null ? new ArrayList<>() : orderRecords;
    }

    @Transactional
    public void saveOrder(Customer customer, Payment payment, List<ShoppingCartItem> items) {
        List<OrderItem> orderItems = new ArrayList<>(items.size());
        double total = 0;
        for(ShoppingCartItem shopItem : items) {
            long inventoryItemId = shopItem.getInventoryItemId();
            InventoryItem item = inventoryItemRepository.findById(inventoryItemId);
            if(item.getQuantity() < shopItem.getQuantity()) {
                System.out.println("NOT ENOUGH QUANTITY TO ORDER!\n");
                return;
            }
            Seller seller = sellerRepository.findById(item.getSellerId());

            OrderItem newOrderItem = new OrderItem();
            newOrderItem.setItemId(item.getItemId())
                    .setPrice(item.getPrice())
                    .setQuantity(shopItem.getQuantity())
                    .setSellerName(seller.getCompanyName());
            orderItems.add(newOrderItem);
            total += item.getPrice();
            inventoryItemRepository.updateOrderItemQuantity(inventoryItemId, item.getQuantity() - shopItem.getQuantity());
        }

        OrderRecord orderRecord = new OrderRecord();
        orderRecord.getAddress().setStreetAddress(customer.getPerson().getAddress().getStreetAddress())
                                .setCity(customer.getPerson().getAddress().getCity())
                                .setState(customer.getPerson().getAddress().getState())
                                .setZipCode(customer.getPerson().getAddress().getZipCode());
        orderRecord.setCustomerId(customer.getId())
                   .setCardInfo(payment.getCreditCard().getCreditCardNumber())
                   .setOrderDate(new Date())
                    .setShippingFee(total * SHIPPING_RATE)
                    .setTax(total * TAX_RATE);

        orderRecordRepository.save(orderRecord);

        for(OrderItem orderItem : orderItems) {
            orderItem.setOrderRecordId(orderRecord.getId());
            orderItemRepository.save(orderItem);
        }

        shoppingCartItemService.deleteAllShoppingCartItems(customer);
    }

}
