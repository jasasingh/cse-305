package com.ministore.ministore.services;

import com.ministore.ministore.model.*;
import com.ministore.ministore.repositories.PaymentRepository;
import com.ministore.ministore.repositories.ShoppingCartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 12/8/2017.
 */
@Service
public class ShoppingCartItemService {

    @Autowired
    private ShoppingCartItemRepository shoppingCartItemRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private OrderService orderService;


    @Transactional(readOnly = true)
    public List<HashMap<String, Object>> getShoppingCartItems(Customer customer) {
        List<HashMap<String, Object>> shoppingCartItems
                = shoppingCartItemRepository.findAllByCustomerId_view(customer.getId());
        return shoppingCartItems == null ? new ArrayList<>() : shoppingCartItems;
    }

    public void deleteAllShoppingCartItems(Customer customer) {
        List<ShoppingCartItem> cartItems = shoppingCartItemRepository.findAllByCustomerId(customer.getId());
        for(ShoppingCartItem cartItem : cartItems) {
            deleteFromShoppingCart(cartItem.getId());
        }
    }

    public void deleteFromShoppingCart(long shoppingCartId) {
        shoppingCartItemRepository.delete(shoppingCartId);
    }

    public void addtoShoppingCart(Customer customer, long inventoryId) {

        List<ShoppingCartItem> cartItems = shoppingCartItemRepository.findAllByCustomerId(customer.getId());

        // simply just add plus 1 to the quantity in the shopping cart if the item is already in the shopping cart
        if(cartItems != null) {
            for(ShoppingCartItem item : cartItems) {
                if(item.getInventoryItemId() == inventoryId) {
                    shoppingCartItemRepository.updateShoppingCartItemQuantity(item.getId(),
                            item.getQuantity() + 1);
                    return;
                }
            }
        }

        ShoppingCartItem shoppingCartItem = new ShoppingCartItem();
        shoppingCartItem.setQuantity(1)
                .setInventoryItemId(inventoryId)
                .setCustomerId(customer.getId());
        System.out.println(customer.getId());
        shoppingCartItemRepository.save(shoppingCartItem);
    }

    public int checkout(Customer customer) {
        List<ShoppingCartItem> shoppingCartItems = shoppingCartItemRepository.findAllByCustomerId(customer.getId());
        if(shoppingCartItems == null || shoppingCartItems.isEmpty()) {
            return 1;
        }

        List<Payment> payments = paymentRepository.findAllByCustomerId(customer.getId());
        if(payments == null || payments.isEmpty()) {
            return 2;
        }

        orderService.saveOrder(customer, payments.get(0), shoppingCartItems);

        return 0;
    }
}
