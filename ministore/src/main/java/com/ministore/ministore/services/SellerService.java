package com.ministore.ministore.services;

import com.ministore.ministore.model.Seller;
import com.ministore.ministore.repositories.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Jaspreet on 12/5/2017.
 */
@Service
public class SellerService {
    
    @Autowired
    private SellerRepository sellerRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public Seller save(Seller seller) {
        seller.setPassword(passwordEncoder.encode(seller.getPassword()));

        sellerRepository.save(seller);
        return seller;
    }

    @Transactional(readOnly = true)
    public Seller findByEmailId(String emailId) {
        return sellerRepository.findByEmailId(emailId);
    }
    
}
