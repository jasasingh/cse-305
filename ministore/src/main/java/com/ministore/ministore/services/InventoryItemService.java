package com.ministore.ministore.services;

import com.ministore.ministore.model.*;
import com.ministore.ministore.repositories.InventoryItemRepository;
import com.ministore.ministore.repositories.ShoppingCartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 12/8/2017.
 */
@Service
public class InventoryItemService {

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @Autowired
    ShoppingCartItemRepository shoppingCartItemRepository;

    public List<HashMap<String, Object>> getAllInventoryItems() {
        List<HashMap<String, Object>> items = inventoryItemRepository.findAll_view();
        return items == null ? new ArrayList<>() : items;
    }

}
