package com.ministore.ministore.services.authorization;

/**
 * Created by Jaspreet on 12/6/2017.
 */
public enum Role {
    ROLE_CUSTOMER(1), ROLE_SELLER(2);
    private final int value;
    private Role(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
