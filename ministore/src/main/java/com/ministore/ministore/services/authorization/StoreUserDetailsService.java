package com.ministore.ministore.services.authorization;

import com.ministore.ministore.model.Customer;
import com.ministore.ministore.model.Seller;
import com.ministore.ministore.repositories.CustomerRepository;
import com.ministore.ministore.repositories.SellerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StoreUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StoreUserDetailsService.class);

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private SellerRepository sellerRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String emailId) throws UsernameNotFoundException {
        // NOTE: we could just search both the seller and customer repository using a UserRepository,
        // but this way will comply more with assignment grading
        Customer customer = customerRepository.findByEmailId(emailId);
        if(customer == null) {
            Seller seller = sellerRepository.findByEmailId(emailId);
            if(seller == null) {
                LOGGER.debug("User not found with the email id " + emailId);
                System.out.println("User not found!");
                throw new UsernameNotFoundException("User not found with the email id " + emailId);
            } else {
                return new SellerDetails(seller);
            }
        } else {
            return new CustomerDetails(customer);
        }
    }

}
