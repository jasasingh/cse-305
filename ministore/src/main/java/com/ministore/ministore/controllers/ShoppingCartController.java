package com.ministore.ministore.controllers;

import com.ministore.ministore.model.Customer;
import com.ministore.ministore.services.OrderService;
import com.ministore.ministore.services.ShoppingCartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Jaspreet on 12/8/2017.
 */
@Controller
@RequestMapping("/cart")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartItemService shoppingCartItemService;

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public String getShoppingCart(Model model, @AuthenticationPrincipal Customer customer) {
        model.addAttribute("emailId", customer.getEmailId());

        model.addAttribute("cart_items", shoppingCartItemService.getShoppingCartItems(customer));

        return "/customer/cart";
    }

    @RequestMapping(path = "/remove", method = RequestMethod.GET)
    public String removeFromShoppingCart(Model model, @RequestParam(value = "item_id") long shoppingCartId, @AuthenticationPrincipal Customer customer) {
        shoppingCartItemService.deleteFromShoppingCart(shoppingCartId);
        model.addAttribute("emailId", customer.getEmailId());

        model.addAttribute("cart_items", shoppingCartItemService.getShoppingCartItems(customer));
        return "/customer/cart";
    }

    @RequestMapping(path = "/add", method = RequestMethod.GET)
    public String addToShoppingCart(Model model, @RequestParam(value = "item_id") long inventoryId, @AuthenticationPrincipal Customer customer) {
        shoppingCartItemService.addtoShoppingCart(customer, inventoryId);
        model.addAttribute("emailId", customer.getEmailId());

        model.addAttribute("cart_items", shoppingCartItemService.getShoppingCartItems(customer));
        return "/customer/cart";
    }

    @RequestMapping(path = "/checkout", method = RequestMethod.GET)
    public String checkout(Model model, @AuthenticationPrincipal Customer customer) {
        int retCode = shoppingCartItemService.checkout(customer);
        switch (retCode) {
            case 1:         // cart was empty
                return "redirect:/cart/get";
            case 2:         // payment was missing
                return "redirect:/customer/addPayment";
            default:
                return "redirect:/orders";
        }
    }


}
