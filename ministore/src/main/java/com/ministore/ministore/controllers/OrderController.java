package com.ministore.ministore.controllers;

import com.ministore.ministore.model.Customer;
import com.ministore.ministore.model.OrderItem;
import com.ministore.ministore.model.OrderRecord;
import com.ministore.ministore.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Jaspreet on 12/8/2017.
 */
@Controller
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(method = RequestMethod.GET)
    public String getOrderRecords(Model model, @AuthenticationPrincipal Customer customer) {
        model.addAttribute("emailId", customer.getEmailId());
        List<OrderRecord> orderRecords =  orderService.getAllRecords(customer);
        model.addAttribute("order_records", orderRecords);
        return "customer/orders";
    }

    @RequestMapping(path = "/orderitems", method = RequestMethod.GET)
    public String getOrderItems(@RequestParam(name = "order_record_id")long orderRecordId, Model model, @AuthenticationPrincipal Customer customer) {
        model.addAttribute("emailId", customer.getEmailId());
        List<OrderItem> orderItems = orderService.getAllOrderItems(customer, orderRecordId);
        model.addAttribute("order_items", orderItems);
        System.out.println(orderItems.size());
        return "customer/order_items";

    }

}
