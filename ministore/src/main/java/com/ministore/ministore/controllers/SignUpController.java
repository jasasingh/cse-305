package com.ministore.ministore.controllers;

import com.ministore.ministore.model.Customer;
import com.ministore.ministore.model.Seller;
import com.ministore.ministore.services.SellerService;
import com.ministore.ministore.services.authorization.CustomerDetails;
import com.ministore.ministore.services.CustomerService;
import com.ministore.ministore.services.authorization.SellerDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/signup")
public class SignUpController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SellerService sellerService;

    @Autowired
    public SignUpController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping(path = "/customer", method = RequestMethod.GET)
    public String customerSignupForm(Model model) {
        Customer newCustomer = new Customer();
        model.addAttribute("customerForm", newCustomer);

        return "customer/signup";
    }

    @RequestMapping(path = "/customer", method = RequestMethod.POST)
    public String customerSignup(@ModelAttribute("customerForm") @Valid Customer customer, BindingResult result,
                         RedirectAttributes redirect) {

        if (result.hasErrors()) {
            return "customer/signup";
        }
        customer = customerService.save(customer);

        if(customer == null){
            return "customer/signup";
        }
        redirect.addFlashAttribute("globalMessage", "Successfully signed up");

        CustomerDetails customerDetails = new CustomerDetails(customer);
        Authentication auth =
                new UsernamePasswordAuthenticationToken(customerDetails, customerDetails.getPassword(), customerDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
        return "redirect:/customer/welcome";
    }

    @RequestMapping(path = "/seller", method = RequestMethod.GET)
    public String sellerSignupForm(Model model) {
        Seller newSeller = new Seller();
        model.addAttribute("sellerForm", newSeller);
        return "seller/signup";
    }

    @RequestMapping(path = "/seller", method = RequestMethod.POST)
    public String sellerSignup(@ModelAttribute("sellerForm") @Valid Seller seller, BindingResult result,
                         RedirectAttributes redirect) {

        if (result.hasErrors()) {
            return "/seller/signup";
        }

        System.out.println("SAVING SELLER!");
        seller = sellerService.save(seller);
        redirect.addFlashAttribute("globalMessage", "Successfully signed up");

        SellerDetails sellerDetails = new SellerDetails(seller);
        Authentication auth =
                new UsernamePasswordAuthenticationToken(sellerDetails, sellerDetails.getPassword(), sellerDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
        return "redirect:/seller/welcome";
    }
}
