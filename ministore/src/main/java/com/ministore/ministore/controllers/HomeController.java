package com.ministore.ministore.controllers;

import com.ministore.ministore.model.Customer;
import com.ministore.ministore.model.Payment;
import com.ministore.ministore.model.Seller;
import com.ministore.ministore.model.User;
import com.ministore.ministore.model.composition.CreditCard;
import com.ministore.ministore.services.CustomerService;
import com.ministore.ministore.services.InventoryItemService;
import com.ministore.ministore.services.authorization.CustomerDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jaspreet on 11/21/2017.
 */
@Controller
public class HomeController {

    @Autowired
    private InventoryItemService inventoryItemService;

    @Autowired
    private CustomerService customerSerivce;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(ModelMap model, @AuthenticationPrincipal User user) {
        if(user instanceof Customer) {
            return "redirect:/" + customerHome(model, (Customer) user);
        } else if(user instanceof Seller) {
            return "redirect:/" + sellerHome(model, (Seller) user);
        }
        return "/error";
    }

    @RequestMapping(value = "/customer/welcome", method = RequestMethod.GET)
    public String customerHome(ModelMap model, @AuthenticationPrincipal Customer customer) {
        model.addAttribute("emailId", customer.getEmailId());

        // Inventory items are passed in an array of lists due to the structure of the model
        // not ideal but whatevs
        // Note that each list is the same length and each object in each list is associated
        // with objects at the same index in the other lists
        List<HashMap<String, Object>> inventoryItems = inventoryItemService.getAllInventoryItems();

        model.addAttribute("inventory", inventoryItems);

        return "customer/welcome";
    }

    @RequestMapping(path = "/customer/addPayment", method = RequestMethod.GET)
    public String addPayment(Model model, @AuthenticationPrincipal Customer customer) {
        CreditCard creditCard = new CreditCard();
        model.addAttribute("emailId", customer.getEmailId());
        model.addAttribute("paymentInfo", creditCard);

        return "customer/info";
    }

    @RequestMapping(path = "/customer/addPayment", method = RequestMethod.POST)
    public String customerSignup(@ModelAttribute("paymentInfo") @Valid CreditCard creditCard, BindingResult result,
                                 RedirectAttributes redirect, @AuthenticationPrincipal Customer customer) {

        if (result.hasErrors()) {
            return "redirect:/customer/addPayment";
        }

        Payment payment = new Payment();
        payment.setCreditCard(creditCard);
        customerSerivce.addPayment(customer, payment);

        return "redirect:/cart/get";
    }

    @RequestMapping(value = "/seller/welcome", method = RequestMethod.GET)
    public String sellerHome(ModelMap model, @AuthenticationPrincipal Seller seller) {
        model.addAttribute("emailId", seller.getEmailId());
        return "seller/welcome";
    }

}
