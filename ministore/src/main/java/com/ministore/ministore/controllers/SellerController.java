package com.ministore.ministore.controllers;

import com.ministore.ministore.databeans.ItemBean;
import com.ministore.ministore.model.Seller;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by Jaspreet on 12/8/2017.
 */
@Controller
@Repository("/seller")
public class SellerController {


    @RequestMapping(path = "/welcome", method = RequestMethod.GET)
    public String getSetItemPage(Model model) {
        ItemBean itemBean = new ItemBean();
        model.addAttribute("itemBean", itemBean);
        return "seller/welcome";
    }

    @RequestMapping(path = "/welcome", method = RequestMethod.POST)
    public String getItemPagePost(@ModelAttribute("itemBean") ItemBean itemBean, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        return "seller/welcome2";
    }
}
