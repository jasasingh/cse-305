package com.ministore.ministore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
// @EnableJpaRepositories(basePackages = "com.ministore.ministore.repositories")
public class MinistoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinistoreApplication.class, args);
	}
}
