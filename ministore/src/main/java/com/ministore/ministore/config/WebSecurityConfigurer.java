package com.ministore.ministore.config;

import com.ministore.ministore.services.authorization.StoreUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers("/js/**","/css/**", "/img/**").permitAll()
                .antMatchers("/signup/**").permitAll()
                .antMatchers("/login/**").anonymous()
                .antMatchers("/customer/**").hasRole("CUSTOMER")
                .antMatchers("/cart/**").hasRole("CUSTOMER")
                .antMatchers("/orders/**").hasRole("CUSTOMER")
                .antMatchers("/seller/**").hasRole("SELLER")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                    .loginPage("/login")
                    .successHandler(new LoginAuthenticationSuccessHandler())
                    .usernameParameter("emailId")
                    .passwordParameter("password")
                    .permitAll()
                .and()
                .logout()
                .permitAll();

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth, StoreUserDetailsService storeUserDetailsService) throws Exception {
        auth
                .userDetailsService(storeUserDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
