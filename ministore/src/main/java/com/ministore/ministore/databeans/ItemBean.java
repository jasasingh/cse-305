package com.ministore.ministore.databeans;

/**
 * Created by Jaspreet on 12/8/2017.
 */
public class ItemBean {

    private String itemName;

    private String description;

    private double price;

    private int quantity;

    public String getItemName() {
        return itemName;
    }

    public ItemBean setItemName(String itemName) {
        this.itemName = itemName;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ItemBean setDescription(String description) {
        this.description = description;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public ItemBean setPrice(double price) {
        this.price = price;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public ItemBean setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }
}
